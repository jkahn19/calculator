
# Calculator App with PySide

A simple calculator app develop with Python using Qt module for Python.


## Installation

Install with Pip in a virtual environment Python for Windows and Linux. In Linux installation, you have the considerer to use 'python3'.

Clone the repository
```bash
  git clone git@gitlab.com:jkahn19/calculator.git
```

Go to the project directory
```bash
  cd calculator
```

Create and Activate a virtual environment with the venv library
```bash
  python -m venv venv
  source venv/Scripts/activate
```

Install dependencies using the requirements file
```bash
  pip install -r requirements.txt
```

Run main.py file as usual python script
```bash
  python main.py
```

## Authors

- [@jkahn19](https://gitlab.com/jkahn19)
