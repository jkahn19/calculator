import time
from PySide6.QtWidgets import QWidget, QPushButton, QLabel, QVBoxLayout, QHBoxLayout


class Calculadora(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Calculadora")

        # BOTONES DE NUMEROES
        button_0 = QPushButton("0")
        button_0.clicked.connect(self.numeropresionado)
        button_1 = QPushButton("1")
        button_1.clicked.connect(self.numeropresionado)
        button_2 = QPushButton("2")
        button_2.clicked.connect(self.numeropresionado)
        button_3 = QPushButton("3")
        button_3.clicked.connect(self.numeropresionado)
        button_4 = QPushButton("4")
        button_4.clicked.connect(self.numeropresionado)
        button_5 = QPushButton("5")
        button_5.clicked.connect(self.numeropresionado)
        button_6 = QPushButton("6")
        button_6.clicked.connect(self.numeropresionado)
        button_7 = QPushButton("7")
        button_7.clicked.connect(self.numeropresionado)
        button_8 = QPushButton("8")
        button_8.clicked.connect(self.numeropresionado)
        button_9 = QPushButton("9")
        button_9.clicked.connect(self.numeropresionado)

        # BOTONES DE OPERACIONES
        button_adici = QPushButton("+")
        button_adici.clicked.connect(self.simbolopresionado)

        button_menos = QPushButton("-")
        button_menos.clicked.connect(self.simbolopresionado)
        button_multi = QPushButton("*")
        button_multi.clicked.connect(self.simbolopresionado)
        button_divi = QPushButton("/")
        button_divi.clicked.connect(self.simbolopresionado)


        # BOTON DE PUNTO
        button_punto = QPushButton(".")
        button_punto.clicked.connect(self.numeropresionado)

        # BOTON DE IGUAL
        button_igual = QPushButton("=")
        button_igual.clicked.connect(self.igualpresionado)

        # BOTON DE BORRAR
        button_borrar = QPushButton("Borrar")
        button_borrar.clicked.connect(self.borrar)

        # BOTON DE CLEAN
        button_clean = QPushButton("CLEAN")
        button_clean.clicked.connect(self.clean)

        # LABEL DE RESULTADOS
        self.label_resultado = QLabel("")

        # LAYOUT GENERAL
        h0Layout = QHBoxLayout()
        h0Layout.addWidget(button_borrar)
        h0Layout.addWidget(button_clean)

        h1Layout = QHBoxLayout()
        h1Layout.addWidget(button_7)
        h1Layout.addWidget(button_8)
        h1Layout.addWidget(button_9)
        h1Layout.addWidget(button_divi)

        h2Layout = QHBoxLayout()
        h2Layout.addWidget(button_4)
        h2Layout.addWidget(button_5)
        h2Layout.addWidget(button_6)
        h2Layout.addWidget(button_multi)

        h3Layout = QHBoxLayout()
        h3Layout.addWidget(button_1)
        h3Layout.addWidget(button_2)
        h3Layout.addWidget(button_3)
        h3Layout.addWidget(button_menos)

        h4Layout = QHBoxLayout()
        h4Layout.addWidget(button_0)
        h4Layout.addWidget(button_punto)
        h4Layout.addWidget(button_igual)
        h4Layout.addWidget(button_adici)

        generalLayout = QVBoxLayout()

        generalLayout.addWidget(self.label_resultado)
        generalLayout.addLayout(h0Layout)
        generalLayout.addLayout(h1Layout)
        generalLayout.addLayout(h2Layout)
        generalLayout.addLayout(h3Layout)
        generalLayout.addLayout(h4Layout)

        self.setLayout(generalLayout)

    def numeropresionado(self):
        buton = self.sender()
        actual = self.label_resultado.text()
        text_buton = buton.text()
        nuevo = actual + text_buton
        self.label_resultado.setText(nuevo)

    def simbolopresionado(self):
        buton = self.sender()
        text_buton = buton.text()
        actual = self.label_resultado.text()
        last_char = actual[-1]

        # Evaluate simbol
        status = True
        operations = '+-*/'
        bolean_symbol = last_char in operations
        bolean_dot = last_char == '.' and actual[-2] in operations
        if bolean_dot or bolean_symbol:
            status = False

        if status:
            nuevo = actual + text_buton
            self.label_resultado.setText(nuevo)

    def igualpresionado(self):
        terms_sumatory = self.label_resultado.text()

        bolean_sum = True if '+' in terms_sumatory else False
        bolean_men = True if '-' in terms_sumatory else False

        bolean_mul = True if '*' in terms_sumatory else False
        bolean_div = True if '/' in terms_sumatory else False

        expresion = terms_sumatory.split('+')

        if bolean_sum:
            expresion_sum = []
            for each_term in terms_sumatory.split('+'):
                if len(each_term) > 0:
                    expresion_sum.append(each_term)
            expresion = expresion_sum
        if bolean_men:
            expresion_men = []
            for each_term in expresion:
                if '-' in each_term:
                    negative_terms = each_term.split('-')
                    if len(negative_terms[0]) > 0 and len(negative_terms[1]) > 0:
                        expresion_men.append(negative_terms[0])
                        expresion_men.append("-"+negative_terms[1])
                    else:
                        expresion_men.append(each_term)
                else:
                    expresion_men.append(each_term)
            expresion = expresion_men
        if bolean_mul or bolean_div:
            for each_index in range(len(expresion)):
                each_term = expresion[each_index]

                valor_auxiliar = 1.
                operation = '*'

                if '*' in each_term or '/' in each_term:
                    longitud = len(each_term)
                    index_1 = 0
                    for index in range(longitud):
                        last = True if index + 1 == longitud else False
                        if each_term[index] == "*" or each_term[index] == '/' or last:
                            if each_term[index] == "*":
                                operation_next = '*'
                            elif each_term[index] == "/":
                                operation_next = '/'
                            try:
                                valor = float(each_term[index_1:]) if last else float(each_term[index_1:index])
                            except ValueError as e:
                                self.errorValue()
                                return
                            if operation == '*':
                                valor_auxiliar *= valor
                            elif operation == '/':
                                if valor != 0:
                                    valor_auxiliar /= valor
                                else:
                                    self.label_resultado.setText('Nan')
                                    return

                            index_1 = index + 1
                            operation = operation_next

                    expresion[each_index] = valor_auxiliar

        try:
            resultado = sum([float(x) for x in expresion])
            self.label_resultado.setText(str(resultado))
        except ValueError as e:
            self.errorValue()
            return

    def errorValue(self):
        self.label_resultado.setText('Error')
        time.sleep(3)
        self.label_resultado.setText('')

    def borrar(self):
        auxiliar = self.label_resultado.text()
        self.label_resultado.setText(auxiliar[:-1])

    def clean(self):
        self.label_resultado.setText("")