import sys

from PySide6.QtWidgets import QApplication

from src.calculadora import Calculadora

app = QApplication(sys.argv)

window = Calculadora()
window.show()

app.exec()
